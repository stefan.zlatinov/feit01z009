import random
from matplotlib import pyplot as plt


simulation_seconds = 100
simulation_time = range(simulation_seconds)


def normalize_belief(belief):
    normalizer = sum(belief.values())
    for state in belief:
        belief[state] /= normalizer
    return belief


def doormen_robot_simulation():
    states = ['open', 'closed']
    # controls = ['push', 'do_nothing']

    # key = (new_state, control_input, current_state)
    state_transition_p = {('open', 'push', 'open'): 1,
                          ('closed', 'push', 'open'): 0,
                          ('open', 'push', 'closed'): 0.8,
                          ('closed', 'push', 'closed'): 0.2,
                          ('open', 'do_nothing', 'open'): 1,
                          ('closed', 'do_nothing', 'open'): 0,
                          ('open', 'do_nothing', 'closed'): 0,
                          ('closed', 'do_nothing', 'closed'): 1}

    # key = (measurement, current_state)
    measurement_p = {('open', 'open'): 0.6,
                     ('closed', 'open'): 0.4,
                     ('open', 'closed'): 0.2,
                     ('closed', 'closed'): 0.8}

    belief = {'open': 0.5, 'closed': 0.5}
    assert sum(belief.values()) == 1, 'Total belief not equal to 1'

    belief_history = [belief['open']]
    for _ in simulation_time:
        u = 'do_nothing'
        z = random.choices(states, [0.6, 0.4])[0]
        for new_state in states:
            belief_after_control_input = 0
            for state in states:
                belief_after_control_input += state_transition_p[(new_state, u, state)] * belief[state]
            belief[new_state] = measurement_p[(z, new_state)] * belief_after_control_input
        belief = normalize_belief(belief)
        belief_history.append(belief['open'])

    plt.plot(simulation_time, belief_history[:-1])
    plt.xlabel('Време')
    plt.ylabel('Веројатност за отворена врата')
    plt.title('Баесов филтер за мерењата на роботот вратар')
    plt.show()


doormen_robot_simulation()
